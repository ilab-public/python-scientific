# Use the official latest python 3.11 bullseye image (currently 3.11.9-bullseye)
FROM python:3.11-bullseye

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get -y upgrade \
 && apt-get install -y --no-install-recommends \
# required for ciso8601, ...
    g++ \
 && rm -rf /var/lib/apt/lists/* \
 && python -m venv /opt/venv

ENV MPLBACKEND=agg \
# Enable python traceback dump for SIGSEGV, SIGFPE, SIGABRT, SIGBUS and SIGILL signals
    PYTHONFAULTHANDLER=1 \
# Prevent python from writing pyc files
    PYTHONDONTWRITEBYTECODE=1 \
# Prevent python from buffering stdout and stderr
    PYTHONUNBUFFERED=1 \
# Make sure we use the virtualenv
    PATH="/opt/venv/bin:$PATH"

COPY pyproject.toml .
COPY requirements.txt .

# steps below will always be executed if `CACHE_DATE` is changed to a new value
ARG CACHE_DATE=2024-04-22
RUN pip install --no-cache-dir --upgrade pip \
 && pip install --no-cache-dir --upgrade -r requirements.txt

CMD ["/bin/bash"]
ENTRYPOINT []
